<?php
	require_once "Vehicle.php";
	
	class Truck extends Vehicle
	{
		public function __construct()
		{
			$this->name = "Truck";
			$this->wheel = "4";
			$this->color = "Yellow";
			$this->brand = "Tata";
			//$this->fuelStock = 1;
			$this->minFuelConsumption = 0.10;
		}
		function load()
		{
			return "";
		}
		function unload()
		{
			return "";
		}
		function design()
		{
			return "";
		}
	}
?>